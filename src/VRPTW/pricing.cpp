#include "pricing.hpp"

Pricing::~Pricing(){
  vector<Route*> vpool(all(pool));
  for(Route* route : vpool)
    delete route;
  
}

double Pricing::solve(int id, int q, int time) {
  if(id == R){
    dp[id][q][time] = {0.0 , curIt};
    return 0.0;
  }

  if(dp[id][q][time].second == curIt)
    return dp[id][q][time].first;

  pair<double, int>& best = dp[id][q][time] = {INT_MAX, curIt};
  for(int i : points[id]){
    int nxTime = max(e[i], time + dist[id][i]);
    int nxQ = d[i] + q;
    int toDepot = nxTime + dist[i][R];

    if(nxQ > capacity or nxTime > l[i] or !nxt[id][i] or toDepot > l[R])
      continue;
    
    assert(id != i);
    double ret = solve(i, nxQ, nxTime) + dual[id][i];
    best.first = min(best.first, ret);
  }
  return best.first;
}

bool Pricing::isElementar(Route* route){
  vi cls = route->cls;
  sort(all(cls));
  cls.resize(unique(all(cls))  - cls.begin());
  return SZ(route->cls) == SZ(cls);
}

void Pricing::addCl(Route* route, int cl){
  if(cl == 0)
    return;
  int last = route->cls.empty() ? 0 : route->cls.back();
  route->val += dual[last][cl];
  if(cl != R){
    for(IM im: clsToSR[cl]){
      int id = im.i;
      seenSR[id] += im.m;
      if(seenSR[id] >= setsSR[id].d){
        route->val += vSR[id];
        seenSR[id] -= setsSR[id].d;
      }
    }
    route->Add(cl);
  }
}

void Pricing::remCl(Route* route, int cl){
  if(cl == 0)
    return;
  if(cl != R){
    assert(route->cls.back() == cl);
    route->Pop();
    for(IM im: clsToSR[cl]){
      int id = im.i;
      seenSR[id] -= im.m;
      if(seenSR[id] < 0){
        route->val -= vSR[id];
        seenSR[id] += setsSR[id].d;
      }
    }
  }
  int last = route->cls.empty() ? 0 : route->cls.back();
  route->val -= dual[last][cl];
}

bool Pricing::recoverMulti(int id, int q, int time, Route* route){
  assert(route->cap <= capacity);

  if(id == R){
    if(forbs.count(route))
      return false;
    assert(route->val < - EPS8);
    assert(route->size() > 0);
    goods.push_back({route->val, new Route(*route)});
    for(int cl : route->cls)
      freqCnt[cl]--;
    for(int cl : route->cls)
      if(freqCnt[cl] <= 0)
        return true;
    return false;
  }

  if(trys-- < 0 && !goods.empty())
    return true;

  if(-trys >= 100'000'000)
    throw "Many iterations in pricing";

  vector<pair<double, int>> childs;

  for(int i : points[id]){
    int nxTime = max(e[i], time + dist[id][i]);
    int nxQ = d[i] + q;
    int toDepot = nxTime + dist[i][R];

    if(nxQ > capacity or nxTime > l[i] or !nxt[id][i] or toDepot > l[R])
      continue;
    
    assert(id != i);
    double val = dp[i][nxQ][nxTime].first;
    assert(dp[i][nxQ][nxTime].second == curIt);

    ld oldVal = route->val;
    addCl(route, i);
    if(route->val + val < -EPS8)
      childs.push_back({route->val + val, i});
    remCl(route, i);
    route->val = oldVal;
  }

  bool ret = false;
  sort(all(childs));
  for(auto pr : childs){
    int i = pr.second;
    if(freqCnt[i] <= 0)
      continue;

    int nxTime = max(e[i], time + dist[id][i]);
    int nxQ = d[i] + q;
    int toDepot = nxTime + dist[i][R];

    if(nxQ > capacity or nxTime > l[i] or !nxt[id][i] or toDepot > l[R])
      continue;
    ld oldVal = route->val;
    addCl(route, i);
    ret |= recoverMulti(i, nxQ, nxTime, route);
    remCl(route, i);
    route->val = oldVal;
    if(ret && (id > 0 || trys < 0))
      break;
  }

  return ret;
}

void Pricing::filterGoods(vector<Route*>& toRet){
  assert(!goods.empty());
  for(auto& dr : goods){
    updateId(dr.second);
    dr.second->updateCost(&G);
    assert(dr.second->val < -EPS8);
    assert(isFeasible(dr.second));
    dr.second->check(&G);
    toRet.push_back(dr.second);
  }
  goods.clear();
}

void Pricing::remPool(Route* route){
  assert(pool.erase(route));
}

vector<Route*> Pricing::getFromPool(){
  vector<Route*> toRet;
  for(Route* route : pool){
    if(!isFeasible(route))
      continue;
    cpm->updateVal(route);
    if(route->val < -EPS8)
      toRet.push_back(route);
  }
  for(Route* route : toRet)
    pool.erase(route);
  return toRet;
}

vector<Route*> Pricing::Solve(bool& opt){
  static int cnt = 0;
  cnt++;

  seenSR.resize(SZ(vSR));
  vector<Route*> toRet = getFromPool();

  if(!toRet.empty()){
    return toRet;
  }
  
  vi ks =  {6, 11, N};
  for(int k : ks){
    curIt++;
    points.assign(N + 1, vi());
    FORI(i, R)
      points[0].push_back(i);

    FORI(i, N)
      if(has[i]){
        FORI(j, N)
          if(i != j)
            points[i].push_back(j);
        vector<pair<ld, int>> valId;
        for(int to : points[i])
          valId.push_back({dual[i][to], to});
        sort(all(valId));
        points[i].clear();

        points[i].push_back(R);
        if(SZ(valId) > k)
          valId.resize(k);
        for(auto pr : valId)
          points[i].push_back(pr.second);
      }

    freqCnt.assign(R + 1, 10);
    double rootValue = solve(0, 0, 0);

    if(rootValue >= -EPS8){
      if(k == N){
        opt = true;
        return {};
      }
      continue;
    }

    Route* temp =  new Route();
    trys = N * capacity;
    recoverMulti(0, 0, 0, temp);
    delete temp;
    
    if(goods.empty()){
      if(k == N){
        opt = true;
        return {};
      }
      continue;
    }
    break;
  }

  checkSets();
  filterGoods(toRet);
  assert(!toRet.empty());

  if(seenSR.empty()){
    ld best = 0;
    for(auto route : toRet)
      best = min(route->val, best);
    assert(fabs(best - dp[0][0][0].first) < EPS8);
  }
  generatedCols += SZ(toRet);
  return toRet;
}

int Pricing::getGenCols(){
  return generatedCols;
}

void Pricing::checkSets(){
  for(Route* route : pool)
    assert(!Route::dels[route->id]);
  for(Route* route : forbs)
    assert(!Route::dels[route->id]);
  for(Route* route : Route::allRoutes)
    assert(!Route::dels[route->id]);
}

bool Pricing::updateId(Route*& route){
  auto iter = Route::allRoutes.find(route);
  if(iter != Route::allRoutes.end()){
    if((*iter)->id == route->id)
      return false;
    (*iter)->val = route->val;
    delete route;
    route = *iter;
  }else{
    route->updateCost(&G);
    Route::allRoutes.insert(route);
  }
  return true;
}

bool Pricing::isFeasible(Route* route){
  vi& cls = route->cls;
  for(int cl : cls)
    if(!has[cl])
      return false;
  if(!nxt[cls.back()][R])
    return false;
  FOR(i, SZ(cls) - 1)
    if(!nxt[cls[i]][cls[i + 1]])
      return false;
  return !forbs.count(route);
}

bool Pricing::inPool(Route* route){
  assert(!updateId(route));
  return pool.count(route);
}

void Pricing::addPool(Route* route){
  assert(!updateId(route));
  assert(route->cost > 0);
  pool.insert(route);
}

void Pricing::addForb(Route* route){
  assert(!updateId(route));
  assert(!forbs.count(route));
  forbs.insert(route);
}

void Pricing::remForb(Route* route){
  assert(!updateId(route));
  assert(forbs.count(route));
  forbs.erase(route);
}
