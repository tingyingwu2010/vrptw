#ifndef CPM_H
#define CPM_H
#include "/opt/gurobi952/linux64/include/gurobi_c++.h"
#include "route.hpp"

struct IM{
  int i, m;
  bool operator <(const IM& im2) const{
    return i != im2.i ? i < im2.i : m < im2.m;
  }
};

struct Cut{
  vector<IM> cls;
  int RHS, d, id = -1;

  Cut(vector<IM> cls, int RHS, int d): cls(cls), RHS(RHS), d(d){}

  bool operator < (const Cut& c) const{
    assert(is_sorted(all(cls)));
    assert(is_sorted(all(c.cls)));
    return d != c.d ? d < c.d : cls < c.cls;
  }

  typedef vector<IM>::iterator iterator;
  typedef vector<IM>::const_iterator const_iterator;

  iterator begin(){
    return cls.begin();
  }

  iterator end(){
    return cls.end();
  }

  const_iterator begin() const{
    return cls.cbegin();
  }

  const_iterator end() const{
    return cls.cend();
  }
};

class CPM{
  GRBModel*& model;
  const int& R;
  bool& bounded;
  const vi &has, &ids;
  const vd &primal;
  const vvd& dual;
  int actCuts = 0, mxActCuts = 0;
  const vector<set<int>> &clToIds;
  vector<vector<IM>> clsToSRAll;
  vector<Cut> setsSRAll;
  vi countSR, seenSR, mapSR;
  set<Cut> cutsPool;
  vector<Cut> lastAdded;

  public:
  vector<vector<IM>> clsToSR;
  vector<Cut> setsSR;
  vd dualSR;

  CPM(GRBModel*& model, const int& R, bool& bounded, const vi& has, const vi& ids, const vd& primal, const vvd& dual, const vector<set<int>> &clToIds);
  int getActCuts();
  int getCuts();
  int getMaxActCuts();
  void updMaxActCuts();
  void addSRCoef(GRBColumn& col, Route* pat);
  ld updateVal(Route* pat);

  void removeLastAdded();
  vector<IM> getIdsCut(Cut cut);
  pair<ld, vector<IM>> calcCut(Cut cut, map<int, int>& invIds);
  multimap<ld, pair<Cut, vector<IM>>, greater<ld>> calcCuts(map<int, int>& invIds);
  int addCutPool(Cut& cut);
  bool genCuts();
  void resetSR();
  int setDualSR(ld val, int cl);
  void checkCuts();
  void clearCuts();
};

#endif
