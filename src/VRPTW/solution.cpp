#include "solution.hpp"

Solution::Solution(Instance* instance): instance(instance), cost(0){}

Solution::Solution(const Solution& sol): instance(sol.instance), cost(0){
  for(auto route: sol.getRoutes())
    addRoute(new Route(*route));
}

Solution::~Solution(){
  for(auto route : routes)
    delete route;
}

void Solution::addRoute(Route* route){
  assert(routes.insert(route).second);
  cost += route->cost;
}

vector<Route*> Solution::getRoutes()const{
  return vector<Route*>(all(routes));
}

void Solution::check(){
  for(auto route: routes)
    route->check(instance);

  vi count(instance->R);
  for(auto route : routes)
    for(int cl : route->cls)
      count[cl]++;

  assert(count[0] == 0);
  FORI(i, instance->n)
    assert(count[i] == 1);
}

void Solution::printSolution(StatusSolution statusSol){
  check();
  ofstream of(instance->prefix + ".sol");
  of << setprecision(6) << fixed << cost  << " " << capacity << "\n";
  int cnt = 0;
  for(auto route : routes){
    of << "Route " << cnt  << ": ";
    cnt++;
    for(auto cl : route->cls)
      of << cl << " ";
    of << " - cap: " << route->cap << "\n";
  }
  of.flush();
  of.close();
}

Solution* Solution::buildSolution(Instance* instance){
  Solution* sol = new Solution(instance);
  sol->fix();
  sol->check();
  return sol;
}

void Solution::findRoute(set<int>& rem, Route* route){
  int cur = 0, time = 0, q = 0;

  while(true){
    ii best = {INT_MAX, -1};
    for(int i : rem){
      int nxTime = max(instance->e[i], time + instance->dist[cur][i]);
      int nxQ = instance->d[i] + q;
      int toDepot = nxTime + instance->dist[i][instance->R];

      if(nxQ > capacity or nxTime > instance->l[i] or toDepot > instance->l[instance->R])
        continue;
      best = min(ii(toDepot, i), best);
    }
    if(best.second == -1)
      break;
    int nxt = best.second;
    q += instance->d[nxt];
    time += instance->dist[cur][nxt];
    time = max(time, instance->e[nxt]);
    cur = nxt;
    route->Add(cur);
    rem.erase(cur);
  }
  route->check(instance);
  route->updateCost(instance);
  assert(route->size() > 0);
}

void Solution::fix(){
  vi seen(instance->R);
  for(auto route : routes)
    for(int cl : route->cls)
      seen[cl]++;

  set<int> rem;   
  FORI(i, instance->n){
    assert(seen[i] <= 1);
    if(!seen[i])
      rem.insert(i);
  }
  while(!rem.empty()){
    Route* route = new Route();
    findRoute(rem, route);
    addRoute(route);
  }
  check();
}

void Solution::remRoute(Route* route){
  auto iter = routes.find(route);
  assert(iter != routes.end());
  route = *iter;
  routes.erase(route);
  cost -= route->cost;
  delete route;
}

int Solution::getSize(){
  return SZ(routes);
}

ll Solution::getCost(){
  return cost;
}
