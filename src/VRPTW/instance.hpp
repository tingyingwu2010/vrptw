#ifndef INSTANCE_H
#define INSTANCE_H

#include "util.hpp"

#define PRECISION 10.0

inline int distAB(int x1, int y1, int x2, int y2) {
  return floor(sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) * PRECISION);
}

struct Instance{
  string path, prefix;
  int n, R;
  vi x, y, e, l, d;
  vvi cost, dist;
  Instance(string path, string prefix, int n): path(path), prefix(prefix), n(n), R(n + 1), 
  x(n + 2), y(n + 2), e(n + 2), l(n + 2), d(n + 2), cost(n + 2, vi(n + 2, INT_MAX / 3)){}

  void Init(vi s){
    FOR(i, R + 1)
      FOR(j, i)
        cost[i][j] = cost[j][i] = distAB(x[i], y[i], x[j], y[j]);
    
    FOR(i, R + 1)
      cost[i][i] = 0;

    FOR(k, R + 1)
      FOR(i, R + 1)
        FOR(j, R + 1)
          cost[i][j] = min(cost[i][j], cost[i][k] + cost[k][j]);
    dist = cost;
    FOR(i, R + 1)
      FOR(j, R + 1)
        dist[i][j] += s[i];
  }

  static Instance* readInstance(string path, string prefix){

    ifstream inStream;

    inStream.open(path);

    if (!inStream.good())
      throw new invalid_argument("Not found instance file");

    int n;
    inStream >> n >> capacity;
    //n = 25;
    Instance* inst = new Instance(path, prefix, n);
    int& R = inst->R;
    vi s(R + 1);
    vi& x = inst->x, &y = inst->y, &e = inst->e, &l = inst->l, &d = inst->d;
    FOR(i, R){
      int id;
      inStream >> id >> x[i] >> y[i] >> d[i] >> e[i] >> l[i] >> s[i];
      e[i] *= PRECISION;
      s[i] *= PRECISION;
      l[i] *= PRECISION;
    }
    x[R] = x[0], y[R] = y[0], e[R] = e[0], l[R] = l[0], s[R] = s[0], d[R] = d[0];

    inst->Init(s);
    inStream.close();

    return inst;
  }
};

#endif
