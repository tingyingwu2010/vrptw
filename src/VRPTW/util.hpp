#ifndef UTIL_H
#define UTIL_H

#include <bits/stdc++.h>

extern int capacity;

using namespace std;
using ii = pair<int, int>;
using vi = vector<int>;
using vvi = vector<vi>;
using ll = long long;
using ld = double;
using vd = vector<ld>;
using vvd = vector<vd>;
using di = pair<double, int>;
using vdi = vector<di>;
using vvdi = vector<vdi>;
#define FOR(i, b)  for(int i = 0; i < (b); i++)
#define REV(i, b)  for(int i = (b) - 1; i >= 0; i--)
#define FORI(i, b) for(int i = 1; i <= (b); i++)
#define all(a) a.begin(), a.end()
#define rall(a) a.rbegin(), a.rend()
#define inside(a) next(a.begin()), prev(a.end())
#define SZ(a) ((int) a.size())
#define EPS8 1e-8
#define EPS9 1e-9
#define EPS13 1e-13

#endif
