#ifndef PRICING_H
#define PRICING_H

#include "instance.hpp"
#include "route.hpp"
#include "CutPlaneManager.hpp"

const int C = 2;

class Pricing {
  CPM* cpm;
  const Instance& G;
  int N, R, generatedCols = 0, curIt = 0;
  const vi x, y, e, l, d;
  const vvi& dist;
  const vvd& dual;
  const vi& has;
  const vvi& nxt;
  const vd& vSR;
  const vector<vector<IM>>& clsToSR;
  const vector<Cut>& setsSR;
  vi seenSR, freqCnt;
  vvi points;

  vector<vector<vector<pair<double, int>>>> dp;
  set<Route*, Route::lessRouteEQ> pool, forbs;
  vector<pair<double, Route*>> goods;

  int trys = 0;

  double solve(int cur, int q, int time);
  bool recoverMulti(int cur, int q, int time, Route* route);
  void filterGoods(vector<Route*>& toRet);
  bool isElementar(Route* route);

  public:

  Pricing(CPM* cpm, const Instance& G, const vvd& dual, const vi& has, const vvi& nxt): 
    cpm(cpm), G(G), N(G.n), R(N + 1), x(G.x), y(G.y), e(G.e), l(G.l),
    d(G.d), dist(G.dist), dual(dual), has(has), nxt(nxt), vSR(cpm->dualSR),
    clsToSR(cpm->clsToSR), setsSR(cpm->setsSR), dp(R + 1, vvdi (capacity + 1, vdi(l[R] + 1, make_pair(0, -1)))){}
  ~Pricing();

  bool isFeasible(Route* route);
  void checkSets();

  void addCl(Route* route, int cl);
  void remCl(Route* route, int cl);
  
  bool updateId(Route*& route);
  bool inPool(Route* route);
  void addPool(Route* route);
  void addForb(Route* route);
  void remForb(Route* route);
  void remPool(Route* route);
  int getGenCols();
  vector<Route*> getFromPool();

  vector<Route*> Solve(bool& opt);
};

#endif
