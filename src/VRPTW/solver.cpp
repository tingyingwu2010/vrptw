#include "solver.hpp"

const int TIMELIMIT = 1800;
#define ROUND(val) (ceil((val) - EPS8))
#define prune(val) (ROUND(val) >= bestInteger)
#define betterEqThan(cur, old) (ROUND(cur) <= ROUND(old))
int capacity;

Solver::Solver(Instance* instance): N(instance->n), R(N + 1), instance(instance),
  clToIds(R, set<int>()), idToRoute(Route::idToRoute), cost(instance->cost), has(R, 1),
  dualP(SZ(cost), vd(SZ(cost[0]))), afim(R, vd(R)), nxt(R + 1, vi(R + 1, 1)), dual(R + 1){

  FOR(i, R + 1)
    nxt[i][i] = 0;

  Route::d = instance->d;
  has[0] = 0;
  cpm = new CPM(model, R, bounded, has, ids, primal, dualP, clToIds);
  pricing = new Pricing(cpm, *instance, dualP, has, nxt);
  partial = new Solution(instance);
  valueStream.open(instance->prefix + ".val");
  initModel();
}

Solver::~Solver(){
  vector<Route*> routes(all(actRoutes));
  for(auto route: routes)
    delete route;

  delete partial;
  delete[] constrs;
  delete[] vars;
  delete model;
  delete pricing;
  delete cpm;
  valueStream.close();
}

void Solver::initModel(){
  try{
    string logPath = instance->prefix + ".gb";
    GRBEnv env = GRBEnv(true);
    env.set("LogFile", logPath);
    env.set(GRB_IntParam_Threads, 1);
    env.set(GRB_DoubleParam_TimeLimit, TIMELIMIT);
    env.set(GRB_IntParam_Presolve, 0);
    env.set(GRB_IntParam_LogToConsole, 0);
    env.set(GRB_IntParam_Method, 0);
    env.set(GRB_DoubleParam_FeasibilityTol, 1e-9);
    env.set(GRB_DoubleParam_OptimalityTol, 1e-9);
    env.set(GRB_IntParam_NumericFocus, 1);
    env.start();

    // Create an empty model
    model = new GRBModel(env);
    model->set(GRB_IntAttr_ModelSense, GRB_MINIMIZE);
    model->set(GRB_IntParam_ScaleFlag, 0);

    begin = chrono::steady_clock::now();

    // Add constraints of covering
    FORI(i, N)
      model->addConstr(GRBLinExpr(), GRB_GREATER_EQUAL, 1.0, "u" + to_string(i));
    model->update();

    bestSol = Solution::buildSolution(instance);
    for(auto r : bestSol->getRoutes()){
      Route* route = new Route(*r);
      pricing->updateId(route);
      assert(addRoute(route));
    }
    bestInteger = bestSol->getCost();
    updateUB();

    FORI(i, N){
      Route* route = new Route(i);
      route->updateCost(instance);
      pricing->updateId(route);
      if(!actRoutes.count(route))
        addRoute(route);
    }

    model->update();
  } catch(GRBException e) {
    cout << "Error code = " << e.getErrorCode() << endl;
    cout << e.getMessage() << endl;
  } catch(...) {
    cout << "Exception during optimization" << endl;
  }
}

bool Solver::addRoute(Route* route){
  assert(!pricing->updateId(route));
  if(actRoutes.count(route)){
    cout << "Repeated Route - Val: " << route->val;
    cpm->updateVal(route);
    cout << " - " << route->val << endl;
    assert(false);
  }
  assert(route->cost != -1);
  assert(pricing->isFeasible(route));
  assert(!pricing->inPool(route));
  //assert(isElementar(route->id));
  route->check(instance);

  GRBColumn col;
  vi cls = route->cls;
  sort(all(cls));
  FOR(i, SZ(cls)){
    int cl = cls[i], f = 1;
    while(i + 1 < SZ(cls) && cls[i] == cls[i + 1]){
      f++;
      i++;
    }
    clToIds[cl].insert(route->id);
    auto constr = model->getConstrByName("u" + to_string(cl));
    col.addTerm(f, constr);
  }

  string varname = "x" + to_string(route->id);
  cpm->addSRCoef(col, route);
  model->addVar(0.0, GRB_INFINITY, route->cost, GRB_CONTINUOUS, col, varname);

  assert(actRoutes.insert(route).second);
  lastVal = route->val;
  return true;
}

template<class T>
void Solver::Unique(vector<T>& v){
  sort(all(v));
  v.resize(unique(all(v))  - v.begin());
}

ld Solver::getObj(){
  return objPrimal;
}

ld Solver::updateTime(){
  chrono::steady_clock::time_point cur = chrono::steady_clock::now();
  return timeCurrent = (chrono::duration_cast<chrono::microseconds>(cur - begin).count() / 1e6L);
}

ld Solver::getTimeFrat(){
  chrono::steady_clock::time_point cur = chrono::steady_clock::now();
  return chrono::duration_cast<chrono::microseconds>(cur - begin).count() / 1e6L;
}

bool Solver::colGen(){
  static int cnt = 0;
  cnt++;
  bool opt = false;
  int R = instance->R;

  FOR(i, R + 1)
    FOR(j, R + 1)
      dualP[i][j] = cost[i][j] - dual[i];

  ld bef = updateTime();
  vector<Route*> routes = pricing->Solve(opt);
  pricingTime += updateTime() - bef;

  if(!opt)
    for(auto route : routes)
      if(!addRoute(route)){
        cout << setprecision(16) << route->val << endl;
        assert(false);
      }
  return opt;
}

int Solver::heaviest(int id){
  int w = 0;
  for(int cl : idToRoute[id]->cls)
    w = max(w, instance->d[cl]);
  return w;
}

void Solver::fixPatsVar(vector<NodeRF>& st){
  static int cnt = 0;
  cnt++;
  vi idsToRem;
  vector<tuple<ld, int, int>> idsFrat;
  FOR(i, M){
    ld val = primal[i];
    int id = ids[i];
    if(isElementar(id)){
      if(val > 1 - EPS8)
        idsToRem.push_back(id);
      else if(val > EPS8)
        idsFrat.push_back({val, -idToRoute[id]->cost, id});
    }
  }
  if(idsToRem.empty()){
    if(!idsFrat.empty()){
      auto pr = *max_element(all(idsFrat));
      idsToRem.push_back(get<2>(pr));
    }else{
      pair<ld, int> best = {EPS8, -1};
      FOR(i, M)
        if(primal[i] > best.first)
          best = {primal[i], ids[i]};
      assert(best.second != -1);
      set<int> useds;
      Route* route = new Route();
      for(int cl : idToRoute[best.second]->cls){
        if(useds.count(cl))
          continue;
        useds.insert(cl);
        route->Add(cl);
      }
      pricing->updateId(route);
      idsToRem.push_back(route->id);
    }
  }
  assert(!idsToRem.empty());

  vi has2 = has;
  vi cls;
  st.push_back(NodeRF());

  FOR(h, SZ(idsToRem)){
    Route* route = idToRoute[idsToRem[h]];
    bool alls = true;
    for(int cl : route->cls)
      alls &= has2[cl] > 0;
    if(alls){
      for(int cl : route->cls)
        has2[cl]--;
      partial->addRoute(new Route(*route));
      st.back().routes.push_back(route);
      st.back().sz++;
      cls.insert(cls.end(), all(route->cls));
    }
  }
  assert(!cls.empty());

  st.back().addCls = cls;
  st.back().addRoutes = remCls(cls);
}

void Solver::remBackNodeRF(vector<NodeRF>& stRF){
  NodeRF& node = stRF.back();

  addCls(node.addCls, node.addRoutes, true);
  for(auto route : node.routes){
    partial->remRoute(route);
    assert(!pricing->updateId(route));
    if(!actRoutes.count(route))
      pricing->addPool(route);
  }
  stRF.pop_back();
}

bool Solver::relaxAndFix(){
  static int cnt = 0;

  cout << "Running Relax and Fix" << endl;
  int iterations = 0, maxIntegers = 0;
  int trys = 0;

  vector<NodeRF> stRF;
  int oldSizePartial = partial->getSize();
  tryAgain:
  ld curObj = Optimize();
  int prevBestInteger = bestInteger;
  maxIntegers = max(maxIntegers, integers);

  while(!optimal){
    assert(bounded);
    iterations++;
    cnt++;

    fixPatsVar(stRF);

    curObj = Optimize();

    if(!bounded || prune(curObj))
      break;

    if(addCuts() || !bounded)
      break;

    if(PH())
      break;
    maxIntegers = max(maxIntegers, integers);
  }

  while(!stRF.empty())
    remBackNodeRF(stRF);

  assert(stRF.empty() && partial->getSize() == oldSizePartial);

  if(!optimal){
    if(bestInteger < prevBestInteger || trys-- > 0){
      prevBestInteger = bestInteger;
      Optimize();
      if(bounded)
        goto tryAgain;
    }

    curObj = Optimize();
    if(bounded){
      PH();
      maxIntegers = max(maxIntegers, integers);
    }
  }

  if(optimal)
    cout << "Find Optimal Solution in Relax and Fix" << endl;
  else
    cout << "Max integers variables: " << maxIntegers << endl;

  assert(partial->getSize() == oldSizePartial);
  return optimal;
}

void Solver::updateLB(){
  if(!bounded)
    return;
  bool alls = true;
  for(auto nd : st)
    alls &= nd.state;
  if(alls && ROUND(getObj()) > bestLB){
    bestLB = ROUND(getObj());
    valueStream << "L " << bestLB / PRECISION << " " << getTimeFrat() << endl;
  }
}

void Solver::updateUB(){
  valueStream << "U " << bestInteger / PRECISION << " " << getTimeFrat() << endl;
}

ld Solver::runPL(){
  try{
    static int cnt = 0;
    repeat:
    cnt++;
    model->update();
    assert(SZ(actRoutes) + masterCol == model->get(GRB_IntAttr_NumVars));
    ld bef = updateTime();
    model->optimize();
    LPTime += updateTime() - bef;
    bounded = model->get(GRB_IntAttr_Status) == GRB_OPTIMAL;

    getPrimal();
    getDual();

    if(masterCol){
      assert(bounded);
      // It can't be worse than using the master column
      assert(model->get(GRB_DoubleAttr_ObjVal) - EPS8 <= 10 * bestInteger);

      auto var = model->getVarByName("M");
      ld val = var.get(GRB_DoubleAttr_X);

      if(val < EPS8){
        model->remove(var);
        masterCol = false;
        goto repeat;
      }
      bounded = false;
      return objPrimal = objDual = INFINITY;
    }

    if(!bounded){
      masterCol = true;
      GRBColumn col;
      int nC = model->get(GRB_IntAttr_NumConstrs);
      FOR(i, nC){
        string name = constrs[i].get(GRB_StringAttr_ConstrName);
        int RHS = constrs[i].get(GRB_DoubleAttr_RHS);
        if(name[0] == 'u'){
          int cl = atoi(&name[1]);
          assert(has[cl] == 1 && RHS == 1);
          col.addTerm(1.0, constrs[i]);
        }
      }
      model->addVar(0.0, GRB_INFINITY, 10 * bestInteger, GRB_CONTINUOUS, col, "M");
      goto repeat;
    }

    objDual += partial->getCost();
    objPrimal += partial->getCost();
    assert(objDual <= objPrimal + EPS8);
    updateIntegers();

    return getObj();
  } catch(GRBException e) {
    cout << "Error code = " << e.getErrorCode() << endl;
    cout << e.getMessage() << endl;
  } catch(...) {
    cout << "Exception during optimization" << endl;
  }
  exit(1);
}

void Solver::clearModel(){
  if(!bounded || ROUND(getObj()) >= bestInteger)
    return;

  vector<Route*> routes(all(actRoutes));
  for(Route* route : routes){
    cpm->updateVal(route);
    if(ROUND(route->val + getObj()) >= bestInteger)
      remRoute(route, false);
  }
  runPL();
}

ld Solver::Optimize(bool cutting){
  cpm->checkCuts();

  int toAbort = cutting ? 50 : INT_MAX;
  int toClear = 100;
  while(true){
    if(updateTime() >= TIMELIMIT)
      throw "Time Limit";
    columnGenerations++;
    runPL();
    print(false);
    if((bounded && PH() && optimal) || --toAbort <= 0)
      break;
    if(bounded && --toClear < 0){
      clearModel();
      toClear = 100;
    }
    try{
      if(colGen())
        break;
    }catch(const char* m){
      cout << m << endl;
      if(cutting){
        toAbort = 0;
        break;
      }else{
        assert(cpm->getActCuts() > 0);
        cpm->clearCuts();
      }
    }
  }

  if(masterCol){
    auto var = model->getVarByName("M");
    model->remove(var);
    model->update();
    masterCol = false;
    bounded = false;
    getPrimal();
  }

  if(!optimal && toAbort > 0){
    checkModelValid();
    clearModel();
  }

  return toAbort > 0 ? getObj() : INT_MIN;
}

void Solver::remRoute(Route* route, bool update){
  bounded = false;
  if(update)
    model->update();

  assert(actRoutes.erase(route));
  auto var = model->getVarByName("x" + to_string(route->id));
  model->remove(var);

  vi cls = route->cls;
  Unique(cls);
  for(int cl : cls)
    assert(clToIds[cl].erase(route->id));

  assert(!pricing->inPool(route));
  pricing->addPool(route);
}

bool Solver::PH(){
  static int cnt = 0;
  cnt++;
  assert(bounded);
  Solution* sol = new Solution(*partial);
  vector<pair<ld, int>> valIds;
  FOR(i, M)
    if(primal[i] > EPS8)
      valIds.push_back({primal[i], ids[i]});

  sort(rall(valIds));

  vi has2 = has;
  for(auto pr : valIds){
    bool all = true;
    vi& cls = idToRoute[pr.second]->cls;
    for(int cl : cls)
      all &= has2[cl];
    if(all){
      Route* route = new Route();
      for(int cl : cls){
        if(has2[cl]){
          route->Add(cl);
          has2[cl]--;
        }
      }
      route->updateCost(instance);
      sol->addRoute(route);
    }
  }
  sol->fix();

  if(*sol < *bestSol){
    swap(sol, bestSol);
    bestInteger = bestSol->getCost();
    cout << "Find Solution - Cost: " << bestSol->getCost() / PRECISION << " - Size: " << bestSol->getSize() << endl;
    updateUB();
  }
  delete sol;
  optimal |= bestInteger == statusSol.L2;
  return bestSol->getCost() <= ROUND(getObj());
}

void Solver::addCls(vi cls, const vi& routes, bool force){
  bounded = false;

  Unique(cls);

  for(int cl : cls){
    GRBLinExpr linExp;
    assert(has[cl] == 0);
    has[cl] = 1;

    model->addConstr(linExp, GRB_GREATER_EQUAL, 1.0, "u" + to_string(cl));
    N++;
  }
  model->update();

  for(auto id : routes)
    if(canBeAdd(idToRoute[id])){
      pricing->remPool(idToRoute[id]);
      addRoute(idToRoute[id]);
    }
    else
      assert(!force);
}

vi Solver::remCls(vi toRem){
  bounded = false;
  model->update();
  Unique(toRem);

  for(int cl : toRem){
    assert(has[cl]);
    has[cl] = 0;
  }

  vi idsToRem;
  for(int cl : toRem)
    for(int id : clToIds[cl])
      if(!pricing->isFeasible(idToRoute[id]))
        idsToRem.push_back(id);

  Unique(idsToRem);
  remRouteList(idsToRem, false);

  for(int cl : toRem){
    auto constr = model->getConstrByName("u" + to_string(cl));
    model->remove(constr);
    N--;
  }
  return idsToRem;
}

void Solver::print(bool force){
  updateTime();
  if(force || timeCurrent - lastPrint >= 5){
    lastPrint = timeCurrent;
    cout << "ObjPD: " << getObj() / PRECISION;
    cout << " - NumCols: " << model->get(GRB_IntAttr_NumVars);
    cout << " - NumCuts: " << cpm->getActCuts();
    cout << " - Time: " << setprecision(2) << timeCurrent << setprecision(12);
    cout << " - lastVal: " << lastVal;
    cout << " - colGens: " << columnGenerations;
    cout << " - Incumbent: " << bestInteger;
    if(lvl > 0){
      cout << "\nStack State: ";
      FOR(i, lvl)
        cout << st[i].state << " "[i % 5 != 4];
    }
    cout << endl;
  }
}

void Solver::checkModelValid(){
  static int cnt = 0;
  assert(!masterCol);
  assert(M == SZ(actRoutes));
  for(Route* route : actRoutes)
    assert(!Route::dels[route->id]);
  pricing->checkSets();
  //model->write("RL.lp");

  FOR(i, M){
    if(ids[i] == -1)
      continue;
    cnt++;
    Route* route = idToRoute[ids[i]];
    assert(!pricing->updateId(route));
    assert(actRoutes.count(route));
    assert(pricing->isFeasible(route));
    cpm->updateVal(route);
    if(primal[i] > EPS8)
      assert(fabs(route->val) < EPS8);
    else
      assert(route->val >= -EPS8);
  }
}

void Solver::updateIntegers(){
  integers = nonZero = partial->getSize();
  if(bounded)
    for(ld val : primal)
      integers += floor(val + EPS8), nonZero += val > EPS8;
}

void Solver::remRouteList(const vi& l, bool update){
  if(update)
    model->update();

  for(int id : l)
    remRoute(idToRoute[id], false);
}

bool Solver::canBeAdd(Route* route){
  return pricing->isFeasible(route);
}

bool Solver::solveRoot(){
  cout << "Initial Heuristic Solution: " << bestInteger << endl;

  ld rootObj = statusSol.rootObj = Optimize();
  statusSol.L1 =  statusSol.L2 = ROUND(rootObj);
  statusSol.rootTime = updateTime();

  print(true);
  updateLB();
  optimal |= bestInteger == statusSol.L1;
  inRoot = false;

  if(!optimal){
    optimal = addCuts();
    statusSol.L2 = ROUND(getObj());
    updateLB();
  }

  cout << "Solved Root" << endl;
  if(optimal){
    cout << "Optimal solution founded while the root was being resolved" << endl;
    return true;
  }
  return optimal;
}

void Solver::swapState(){
  bounded = false;

  Node& node = st.back();
  node.state ^= true;
  int cl1 = node.cl1, cl2 = node.cl2;

  if(node.state){
    nxt[cl1] = node.nxt1;
    nxt[cl2] = node.nxt2;
    nxt[cl1][cl2] = 0;
    for(int id : node.rem)
      if(pricing->isFeasible(idToRoute[id])){
        pricing->remPool(idToRoute[id]);
        addRoute(idToRoute[id]);
      }
    node.rem.clear();
    remConflicts(cl1, cl2, node.rem);
  }else{
    assert(false);
    /*pricing->remForb(route);
    remCls(route->cls);
    partial->addRoute(new Route(*route));*/
  }
}

void Solver::remBackNode(){
  Node& node = st.back();
  assert(node.state);

  nxt[node.cl1] = node.nxt1;
  nxt[node.cl2] = node.nxt2;
  for(int id : node.rem)
    if(pricing->isFeasible(idToRoute[id])){
      pricing->remPool(idToRoute[id]);
      addRoute(idToRoute[id]);
    }
    else
      assert(!isElementar(id));

  st.pop_back();
  lvl--;
  model->update();
}

int Solver::backtracking(){
  int toUp = 0;
  while(!st.empty()){
    if(!st.back().state)
      break;
    remBackNode();
    toUp++;
  }
  if(!st.empty())
    swapState();
  else
    optimal = true;
  return toUp;
}

bool Solver::addCuts(){
  assert(bounded);
  if(!useCut)
    return false;
  int cnt = 0;
  while(cnt++ < 5 && cpm->getActCuts() < 100){
    if(!cpm->genCuts())
      break;
    
    ld curObj = Optimize(true);

    if(curObj == INT_MIN){
      cpm->removeLastAdded();
      Optimize();
      return useCut = false;
    }

    if(prune(curObj)){
      cout << "Pruned by SR Cuts\n";
      return true;
    }
    if(PH() && prune(curObj)){
      cout << "Improved while cutting" << endl;
      return true;
    }
  }
  return false;
}

bool Solver::fixST(ld& curObj){
  assert(!st.empty());

  while(true){
    assert(lvl == SZ(st));
    if(!st.back().state && prune(curObj)){
      cout << "Swaping: " << curObj << " - cl1 " << st.back().cl1 << " - cl2 " << st.back().cl2 << " - ST: " << st.back().state << " - lvl " << lvl << endl;
      swapState();
      curObj = Optimize();
    }
    if(prune(curObj)){
      cout << "Fail: " << curObj  << " - cl1 " << st.back().cl1 << " - cl2 " << st.back().cl2 << " - ST: " << st.back().state;
      cout << " - Subiu " << backtracking() << " niveis" << " - cur lvl " << lvl << endl;
      if(optimal)
        return true;
      curObj = Optimize();
      continue;
    }
    if(addCuts()){
      if(optimal)
        return true;
      curObj = getObj();
      continue;
    }
    break;
  }
  return false;
}

bool Solver::relax(ld& curObj){
  static int cnt = 0;
  cnt++;
  if(cnt % 5 == 0)
    useCut = true;
  if(fixST(curObj))
    return true;
  cout << "Sucess: " << curObj  << " - cl1 " << st.back().cl1 << " - cl2 " << st.back().cl2;
  cout << " - ST: " << st.back().state << " - Integers: " << integers;
  cout << " - NumCols: " << model->get(GRB_IntAttr_NumVars);
  cout << " - Incumb: " << bestInteger;
  cout << " - NumCuts: " << cpm->getActCuts();
  cout << " - lvl " << lvl << endl;
  return false;
}

void Solver::getPrimal(){
  objPrimal = bounded ? model->get(GRB_DoubleAttr_ObjVal): INFINITY;
  M = model->get(GRB_IntAttr_NumVars);
  assert(M == SZ(actRoutes) + masterCol);
  primal.resize(M);
  ids.resize(M);

  if(vars != NULL)
    delete[] vars;
  vars = model->getVars();

  FOR(i, M){
    primal[i] = bounded ? vars[i].get(GRB_DoubleAttr_X) : 0.0;
    string name = vars[i].get(GRB_StringAttr_VarName);
    ids[i] = name[0] == 'x' ? atoi(&name[1]) : -1;
    if(ids[i] != -1)
      assert(actRoutes.count(idToRoute[ids[i]]));
  }
}

void Solver::getDual(){
  objDual = 0.0;
  int N2 = model->get(GRB_IntAttr_NumConstrs);
  if(constrs != NULL)
    delete[] constrs;
  constrs = model->getConstrs();

  fill(all(dual), -1e5);
  dual[0] = dual[R] = 0;
  cpm->resetSR();
  if(!bounded)
    return;

  FOR(i, N2){
    ld pi = constrs[i].get(GRB_DoubleAttr_Pi);
    string name = constrs[i].get(GRB_StringAttr_ConstrName);
    int cl = atoi(&name[1]);
    if(name[0] == 'u'){
      dual[cl] = pi;
      objDual += dual[cl];
    }else if(name[0] == 'c'){
      assert(pi < EPS8);
      int rhs = cpm->setDualSR(pi, cl);
      objDual += rhs * pi;
    }
    else{
      objDual += pi;
    }
  }
  cpm->updMaxActCuts();
}

bool Solver::isElementar(int id){
  Route* route = idToRoute[id];
  vi cls = route->cls;
  Unique(cls);
  return SZ(route->cls) == SZ(cls);
}

void Solver::remConflicts(int cl1, int cl2, vi& toRem){
  for(int id : clToIds[cl1])
    if(!pricing->isFeasible(idToRoute[id]))
      toRem.push_back(id);
  for(int id : clToIds[cl2])
    if(!pricing->isFeasible(idToRoute[id]))
      toRem.push_back(id);
  Unique(toRem);
  remRouteList(toRem, true);
  model->update();
}

void Solver::mergeCls(int cl1, int cl2){
  st.push_back(Node(cl1, cl2));
  lvl++;
  st.back().nxt1 = nxt[cl1];
  st.back().nxt2 = nxt[cl2];

  FORI(i, R)
    nxt[cl1][i] = i == cl2;
  nxt[cl2][cl1] = 0;

  remConflicts(cl1, cl2, st.back().rem);
}

bool Solver::fixOneVar(){
  assert(bounded);
  FOR(i, SZ(afim))
    fill(all(afim[i]), 0);

  FOR(h, M){
    if(primal[h] < EPS8)
      continue;
    vi& cls = idToRoute[ids[h]]->cls;
    FOR(i, SZ(cls) - 1)
      afim[cls[i]][cls[i + 1]] += primal[h];
  }
  pair<ld, ii> best = {0.5, ii(-1, -1)};
  FOR(i, SZ(afim))
    FOR(j, SZ(afim)){
      if(fabs(afim[i][j] - 0.5) < best.first)
        best = {fabs(afim[i][j] - 0.5), ii(i, j)};
    }
  if(best.first >= 0.5)
    return false;

  int cl1 = best.second.first, cl2 = best.second.second;
  mergeCls(cl1, cl2);
  return true;
}

void Solver::BranchAndPrice(){
  cout << "Begin Branch and Price" << endl;

  while(true){
    assert(lvl >= 0);
    iterationsBP++;

    if(iterationsBP % 10 == 0)
      relaxAndFix();

    ld curObj = getObj();

    assert(lvl == SZ(st));

    if(bounded && !fixOneVar()){
      bool any = false;
      FOR(i, M){
        if(primal[i] > EPS8 && primal[i] < 1 - EPS8){
          Route* route = idToRoute[ids[i]];
          if(isElementar(route->id)){
            cout << primal[i] << " - ";
            for(int cl : route->cls)
              cout << cl << " ";
            cout << endl;
            assert(false);
          }
          any = true;
        }
      }
      assert(!any);
    }
    curObj = Optimize();

    if(relax(curObj))
      break;

    updateLB();

    while(true){
      PH();
      if(!prune(curObj))
        break;
      if(relax(curObj))
        goto end;
    }
  }
  end:
  if(optimal)
    objPrimal = objDual = bestInteger;
  updateLB();
  return;
}

void Solver::Solve(){
  try{
    if(!solveRoot())
      BranchAndPrice();
  }catch(const char* m){
    string msg = "";
    msg += m;
    cout << msg << endl;
  }

  statusSol.status =  optimal ? "Optimal" : "Time_Limit";
  statusSol.Z = bestSol->getCost();
  statusSol.finalTime = updateTime();
  if(!optimal)
    updateUB();

  bestSol->printSolution(statusSol);
  print(true);

  cout << setprecision(2) << fixed;
  cout << "IntegerSol: " << bestSol->getCost() / PRECISION << " - Root LB: " << statusSol.L2 / PRECISION;
  cout  << " - Initial LB: " << statusSol.L1  / PRECISION << " - Status: " << statusSol.status;
  cout << " - Time: " << statusSol.finalTime <<  " - GAP: " << (statusSol.Z - bestLB) / bestLB;
  cout << " - genCol: " << pricing->getGenCols();
  cout << " - genCuts: " << cpm->getCuts() << " - mxActCuts: " << cpm->getMaxActCuts();
  cout << endl;
  cout << "Pricing Time: " << pricingTime << " - LP Time: " << LPTime << endl << endl;

  ofstream logStream;
  logStream.open(instance->prefix + ".log");
  logStream << setprecision(2) << fixed;
  logStream << (statusSol.Z - bestLB) / bestLB << ",";
  logStream << statusSol.Z / PRECISION << "," << bestLB / PRECISION << ",";
  logStream << statusSol.rootObj / PRECISION << "," << getTimeFrat()  << ",";
  logStream << pricing->getGenCols() << ",";
  logStream << cpm->getCuts() << endl;
  logStream.close();

  delete bestSol;
  return;
}
