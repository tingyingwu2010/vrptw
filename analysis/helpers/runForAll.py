import os
from glob import glob

def runForAll2(dirHeuristic, instancesDir):
    heuristicName = dirHeuristic[dirHeuristic.rfind('/') + 1:]
    heuristic = dirHeuristic + "/" + heuristicName

    os.system("cd " + dirHeuristic + "; make")

    if not os.path.exists(instancesDir):
        print("Not found the dir of instance")
        exit(1)

    os.system("mkdir " + dirHeuristic + "/out")

    instancesDirName = instancesDir[instancesDir.rfind('/') + 1:]
    logFile = dirHeuristic + "/out/" + instancesDirName + ".log"
    logAll = open(logFile, "w")

    for instance in sorted(glob(os.path.join(instancesDir, "*"))):
        print ("Running instance", instance)
        instanceName = instance[instance.rfind('/') + 1:]
        os.system(heuristic + " " + instance)
        logFileCur = open(dirHeuristic + "/out/" + instanceName + ".log", "r")
        logAll.write(instanceName + ',' + logFileCur.readline())
        logAll.flush()
        logFileCur.close()
        
    return logFile