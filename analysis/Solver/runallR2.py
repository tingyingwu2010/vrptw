

import os
from math import ceil
import time


import sys; sys.path.insert(0, '..')
from helpers.runForAll import *

projectDir = "../../"

instanceRoot = projectDir + "instances"

instancesSuffixes= ["R2"]

alg = projectDir + "src/VRPTW"
logs = [projectDir + "src/VRPTW/out/" + suff + ".log" for suff in instancesSuffixes]

for suf in instancesSuffixes:
  instanceDir = instanceRoot + "/" + suf
  runForAll2(alg, instanceDir)
  time.sleep(0.2)
