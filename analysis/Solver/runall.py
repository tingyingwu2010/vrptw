

import os
from math import ceil


import sys; sys.path.insert(0, '..')
from helpers.runForAll import *

projectDir = "../../"

instanceRoot = projectDir + "instances"

instancesSuffixes= ["C1", "R1", "RC1", "C2", "R2", "RC2"]

alg = projectDir + "src/VRPTW"
logs = [projectDir + "src/VRPTW/out/" + suff + ".log" for suff in instancesSuffixes]

for suf in instancesSuffixes:
  instanceDir = instanceRoot + "/" + suf
  runForAll2(alg, instanceDir)
